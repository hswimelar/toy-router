package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestExtractNameFromV2URL(t *testing.T) {
	var urlPats = []string{
		"/v2/%s/tags/list",
		"/v2/%s/tags/reference/latest",
		"/v2/%s/manifests/reference/latest",
		"/v2/%s/blobs/sha256:1234567890098765432112345667890098765",
		"/v2/%s/blobs/uploads",
		"/v2/%s/blobs/uploads/D95306FA-FAD3-4E36-8D41-CF1C93EF8286",
	}

	var tests = []struct {
		name     string
		wantName string
	}{
		{
			name:     "single segment",
			wantName: "foo",
		},
		{
			name:     "double segment",
			wantName: "foo/bar",
		},
		{
			name:     "name like route ending",
			wantName: "tags/list",
		},
		{
			name:     "name ending in route section",
			wantName: "foo/bar/manifests",
		},
		{
			name:     "invalid slashes",
			wantName: "//bar//",
		},
		{
			name:     "invalid chars",
			wantName: "r€pø/$kunk",
		},
		{
			name:     "empty name",
			wantName: "",
		},
		{
			name:     "single char",
			wantName: "a",
		},
	}

	for _, tt := range tests {
		for _, pat := range urlPats {
			t.Run(tt.name, func(t *testing.T) {
				name := extractNameFromV2URL(fmt.Sprintf(pat, tt.wantName))
				require.Equal(t, tt.wantName, name)
			})
		}
	}
}

// Bad URLs for later
/*
	{
		url:     "/gitlab/v1/repositories/foo%2Fbar%2Fa%2Fa%2Ftags%2Flist",
		id:      v1RepositoryTags.id,
		methods: []string{http.MethodGet},
	},
		{
			url:     "/gitlab/v1/repository-paths/foo%2F/repositories/list",
			id:      v1RepositoryPaths.id,
			methods: []string{http.MethodGet},
		},
*/

func TestRoutes(t *testing.T) {
	var tests = []struct {
		url      string
		methods  []string
		wantResp *testResponse
	}{
		{
			url: "/v2",
			wantResp: &testResponse{
				Name:      "",
				RouteID:   int(v2Base.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/v2/foo/tags/list",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2TagsList.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/v2/foo/tags/reference/latest",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2TagsDelete.id),
				Reference: "latest",
			},
			methods: []string{http.MethodDelete},
		},
		{
			url: "/v2/foo/manifests/latest",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2Manifests.id),
				Reference: "latest",
			},
			methods: []string{http.MethodGet, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/v2/foo/manifests/sha256:1234567890098765432112345667890098765",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2Manifests.id),
				Reference: "sha256:1234567890098765432112345667890098765",
			},
			methods: []string{http.MethodGet, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/v2/foo/blobs/sha256:1234567890098765432112345667890098765",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2Blobs.id),
				Reference: "sha256:1234567890098765432112345667890098765",
			},
			methods: []string{http.MethodGet, http.MethodDelete},
		},
		{
			url: "/v2/foo/blobs/uploads",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2BlobsUploads.id),
				Reference: "",
			},
			methods: []string{http.MethodPost},
		},
		{
			url: "/v2/foo/blobs/uploads/D95306FA-FAD3-4E36-8D41-CF1C93EF8286",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2BlobsUploadsChunk.id),
				Reference: "D95306FA-FAD3-4E36-8D41-CF1C93EF8286",
			},
			methods: []string{http.MethodGet, http.MethodPatch, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/v2/foo/blobs/uploads/RDk1MzA2RkEtRkFEMy00RTM2LThENDEtQ0YxQzkzRUY4Mjg2IA==",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2BlobsUploadsChunk.id),
				Reference: "RDk1MzA2RkEtRkFEMy00RTM2LThENDEtQ0YxQzkzRUY4Mjg2IA==",
			},
			methods: []string{http.MethodGet, http.MethodPatch, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/v2/foo/blobs/uploads/RDk1MzA2RkEtRkFEMy00RTM2LThENDEtQ0YxQzkzRUY4Mjg2IA_-==",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v2BlobsUploadsChunk.id),
				Reference: "RDk1MzA2RkEtRkFEMy00RTM2LThENDEtQ0YxQzkzRUY4Mjg2IA_-==",
			},
			methods: []string{http.MethodGet, http.MethodPatch, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/gitlab/v1",
			wantResp: &testResponse{
				Name:      "",
				RouteID:   int(v1Base.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/foo/tags/list",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v1RepositoryTags.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/foo%2Fbar/tags/list",
			wantResp: &testResponse{
				Name:      "foo/bar",
				RouteID:   int(v1RepositoryTags.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/tags%2Flist%2Ftags%2Flist/tags/list",
			wantResp: &testResponse{
				Name:      "tags/list/tags/list",
				RouteID:   int(v1RepositoryTags.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/v2/_catalog",
			wantResp: &testResponse{
				Name:      "",
				RouteID:   int(v2Catalog.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/foo",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v1Repositories.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/foo%2Fbar",
			wantResp: &testResponse{
				Name:      "foo/bar",
				RouteID:   int(v1Repositories.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/list%2Ftags",
			wantResp: &testResponse{
				Name:      "list/tags",
				RouteID:   int(v1Repositories.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repositories/foo%2Fbar",
			wantResp: &testResponse{
				Name:      "foo/bar",
				RouteID:   int(v1Repositories.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/import/foo",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v1Import.id),
				Reference: "",
			},
			methods: []string{http.MethodGet, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/gitlab/v1/import/foo%2Fbar",
			wantResp: &testResponse{
				Name:      "foo/bar",
				RouteID:   int(v1Import.id),
				Reference: "",
			},
			methods: []string{http.MethodGet, http.MethodPut, http.MethodDelete},
		},
		{
			url: "/gitlab/v1/repository-paths/foo/repositories/list",
			wantResp: &testResponse{
				Name:      "foo",
				RouteID:   int(v1RepositoryPaths.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
		{
			url: "/gitlab/v1/repository-paths/foo%2Fbar/repositories/list",
			wantResp: &testResponse{
				Name:      "foo/bar",
				RouteID:   int(v1RepositoryPaths.id),
				Reference: "",
			},
			methods: []string{http.MethodGet},
		},
	}

	for _, tt := range tests {
		for _, method := range tt.methods {
			t.Run(method+" "+tt.url, func(t *testing.T) {
				req := httptest.NewRequest(method, "http://example.com"+tt.url, nil)

				w := httptest.NewRecorder()
				getRootRouter().ServeHTTP(w, req)

				resp := w.Result()
				defer resp.Body.Close()
				require.Equal(t, resp.StatusCode, http.StatusOK)

				body, err := io.ReadAll(resp.Body)
				require.NoError(t, err)

				tr := &testResponse{}
				require.NoError(t, json.Unmarshal(body, tr))

				require.Equal(t, tt.wantResp.Name, tr.Name)
				require.Equal(t, tt.wantResp.RouteID, tr.RouteID)
				require.Equal(t, tt.wantResp.Reference, tr.Reference)
			})
		}
	}
}

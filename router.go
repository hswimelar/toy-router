package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/docker/distribution/reference"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/opencontainers/go-digest"
)

const (
	urlEncodedNameKey = "urlEncodedName"
	nameKey           = "name"
	tagKey            = "tag"
	referenceKey      = "reference"
	digestKey         = "digest"
	uuidKey           = "uuid"
)

// This single regexp allows us to extract the name from all V2 routes.
// This implementation requires the urlEncoded names for v1 routes as it depends
// on the name being infixed into the URL, surrounded by static segments.
var v2NamePat = regexp.MustCompile(`^/v2/(.+)/(?:manifests|tags|blobs){1}.*$`)

//                                  ^    ^     ^ ^                       ^
// literal start of v2 route -------'    |     | |                       |
// match group for name -----------------'     | |                       |
// non capturing group ------------------------' |                       |
// v2 routes all start with exactly one of       |                       |
// these three segments after the name ----------'	                     |
// we don't care about the rest of the URL for name extraction ----------'

// routeIDs help the tests to track which handler was called, they're not intended
// to make it into the real implementation.
type routeID int

func (r routeID) String() string {
	return fmt.Sprintf("%d", r)
}

const (
	unknownRouteID = iota
	v2BaseID
	v2CatalogID
	v2TagsListID
	v2TagsDeleteID
	v2ManifestsID
	v2BlobsID
	v2BlobsUploadsID
	v2BlobsUploadsChunkID
	v1BaseID
	v1RepositoryTagsID
	v1RepositoriesID
	v1ImportID
	v1RepositoryPathsID
)

type route struct {
	path string
	id   routeID
}

type testResponse struct {
	Name      string `json:"name,omitempty"`
	RouteID   int    `json:"route_id"`
	Reference string `json:"reference,omitempty"`
}

// The is the chi way to write responses. We can choose to do some preprocessing
// here before marshalling, but it's also a relatively pleasant sugar for
// json (un)marshalling.
func (tr *testResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Route definitions
// path is more-or-less what we could use in on registry itself
// The regexp based URL params are the exact same as we use now.
// Note the the v2 routes are defined with the placeholder _name value directly.
var (
	v2Base = route{
		path: "/v2",
		id:   v2BaseID,
	}
	v2Catalog = route{
		path: "/_catalog",
		id:   v2CatalogID,
	}
	v2TagsDelete = route{
		path: "/_name/tags/reference/{" + tagKey + ":" + reference.TagRegexp.String() + "}",
		id:   v2TagsDeleteID,
	}
	v2Manifests = route{
		path: "/_name/manifests/{" + referenceKey + ":" + reference.TagRegexp.String() + "|" + digest.DigestRegexp.String() + "}",
		id:   v2ManifestsID,
	}
	v2Blobs = route{
		path: "/_name/blobs/{" + digestKey + ":" + digest.DigestRegexp.String() + "}",
		id:   v2BlobsID,
	}
	v2BlobsUploads = route{
		path: "/_name/blobs/uploads",
		id:   v2BlobsUploadsID,
	}
	v2BlobsUploadsChunk = route{
		path: "/_name/blobs/uploads/{" + uuidKey + ":[a-zA-Z0-9-_.=]+}",
		id:   v2BlobsUploadsChunkID,
	}
	v2TagsList = route{
		path: "/_name/tags/list",
		id:   v2BaseID,
	}
	v1Base = route{
		path: "/gitlab/v1",
		id:   v1BaseID,
	}
	v1RepositoryTags = route{
		path: "/repositories/{" + urlEncodedNameKey + "}/tags/list",
		id:   v1RepositoryTagsID,
	}
	v1Repositories = route{
		path: "/repositories/{" + urlEncodedNameKey + "}",
		id:   v1RepositoriesID,
	}
	v1Import = route{
		path: "/import/{" + urlEncodedNameKey + "}",
		id:   v1ImportID,
	}
	v1RepositoryPaths = route{
		path: "/repository-paths/{" + urlEncodedNameKey + "}/repositories/list",
		id:   v1RepositoryPathsID,
	}
)

func main() {
	r := getRootRouter()
	http.ListenAndServe(":3000", r)
}

// Here we define the handlers, this is somewhat tedious as we have a lot of
// routes, but I wanted to cover all cases incase there were ordering effects
// with route definitions.
func getRootRouter() *chi.Mux {
	// V2 Routes
	v2Router := chi.NewRouter()
	v2Router.Use(v2Middleware)
	v2Router.Use(v2NameMiddleware)

	v2Router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v2 base handler\n")

		tr := &testResponse{Name: "", RouteID: int(v2Base.id), Reference: ""}
		render.Render(w, r, tr)
	})

	v2Router.Get(v2Catalog.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v2 catalog handler\n")

		render.Render(w, r, &testResponse{Name: "", RouteID: int(v2Catalog.id), Reference: ""})
	})

	// V2 Routes with names need to be manually prefixed with the V2 base path
	// since their URLs are rewritten. This is likely faster and less error-prone
	// than trying to strip the V2 base URL from the rewritten route, but this
	// has not yet been explored.
	v2Router.Get(v2Base.path+v2TagsList.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("tags handler\n")

		// The name is contained within the context, rather than a chi.URLParam since
		// it's extracted with middleware.
		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2TagsList.id), Reference: ""})
	})

	v2Router.Delete(v2Base.path+v2TagsDelete.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("tags DELETE handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		// This is the chi way to get URL params.
		ref := chi.URLParam(r, tagKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2TagsDelete.id), Reference: ref})
	})

	v2Router.Get(v2Base.path+v2Manifests.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("manifest GET handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, referenceKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2Manifests.id), Reference: ref})
	})

	v2Router.Put(v2Base.path+v2Manifests.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("manifest PUT handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, referenceKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2Manifests.id), Reference: ref})
	})

	v2Router.Delete(v2Base.path+v2Manifests.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("manifest DELETE handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, referenceKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2Manifests.id), Reference: ref})
	})

	v2Router.Get(v2Base.path+v2Blobs.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs GET handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, digestKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2Blobs.id), Reference: ref})
	})

	v2Router.Delete(v2Base.path+v2Blobs.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs DELETE handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, digestKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2Blobs.id), Reference: ref})
	})

	v2Router.Post(v2Base.path+v2BlobsUploads.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs Uploads handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2BlobsUploads.id), Reference: ""})
	})

	v2Router.Get(v2Base.path+v2BlobsUploadsChunk.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs Uploads chunk GET handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, uuidKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2BlobsUploadsChunk.id), Reference: ref})
	})

	v2Router.Put(v2Base.path+v2BlobsUploadsChunk.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs Uploads chunk PUT handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, uuidKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2BlobsUploadsChunk.id), Reference: ref})
	})

	v2Router.Delete(v2Base.path+v2BlobsUploadsChunk.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs Uploads chunk DELETE handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, uuidKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2BlobsUploadsChunk.id), Reference: ref})
	})

	v2Router.Patch(v2Base.path+v2BlobsUploadsChunk.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("blobs Uploads chunk PATCH handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)
		ref := chi.URLParam(r, uuidKey)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v2BlobsUploadsChunk.id), Reference: ref})
	})

	// V1 Routes
	v1Router := chi.NewRouter()
	v1Router.Use(v1Middleware)

	v1Router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 base handler\n")

		render.Render(w, r, &testResponse{Name: "", RouteID: int(v1Base.id), Reference: ""})
	})

	// With v1 routes, we can use the With function to only attack the middle ware to these routes and
	// avoid needing to have special cases in the middleware to handle routes without names.
	v1Router.With(v1NameMiddleware).Get(v1RepositoryTags.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 repository tags handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v1RepositoryTags.id), Reference: ""})
	})

	v1Router.With(v1NameMiddleware).Get(v1Repositories.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 repositories handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v1Repositories.id), Reference: ""})
	})

	v1Router.With(v1NameMiddleware).Get(v1Import.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 import GET handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v1Import.id), Reference: ""})
	})

	v1Router.With(v1NameMiddleware).Put(v1Import.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 import PUT handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v1Import.id), Reference: ""})
	})

	v1Router.With(v1NameMiddleware).Delete(v1Import.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 import DELETE handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v1Import.id), Reference: ""})
	})

	v1Router.With(v1NameMiddleware).Get(v1RepositoryPaths.path, func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 repository paths handler\n")

		ctx := r.Context()
		name := ctx.Value(nameKey).(string)

		render.Render(w, r, &testResponse{Name: name, RouteID: int(v1RepositoryPaths.id), Reference: ""})
	})

	rootRouter := chi.NewRouter()
	rootRouter.Use(rootMiddleware)
	//	rootRouter.Use(middleware.RedirectSlashes)
	rootRouter.Mount(v2Base.path, v2Router)
	rootRouter.Mount(v1Base.path, v1Router)

	return rootRouter
}

func v2Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v2 router\n")
		next.ServeHTTP(w, r)
	})
}

func v1Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 router\n")
		next.ServeHTTP(w, r)
	})
}

func rootMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("root router\n")
		fmt.Printf("url: %s\n", r.URL.Path)
		next.ServeHTTP(w, r)
	})
}

// This middleware extracts the urlEncodedName from v1 urls and converts it to
// use slashes.
func v1NameMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v1 name middleware\n")

		urlEncodedName := chi.URLParam(r, urlEncodedNameKey)
		fmt.Printf("urlEncodedName: %s\n", urlEncodedName)

		// TODO: Validate the name here.

		//name := strings.Replace(urlEncodedName, `%2F`, "/", -1)
		name, err := url.QueryUnescape(urlEncodedName)
		if err != nil {
			panic(err)
		}
		fmt.Printf("name: %s\n", name)

		ctx := context.WithValue(r.Context(), nameKey, name)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// This middleware extracts the name v2 paths, based on Jaime's implementation.
func v2NameMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("v2 name middleware\n")

		// We need to use this middleware directly on the router, so we may encounter
		// any v2 route, so we need to exit early for the routes without names.
		switch r.URL.Path {
		case v2Base.path, v2Base.path + v2Catalog.path:
			next.ServeHTTP(w, r)
			return
		}

		name := extractNameFromV2URL(r.URL.Path)
		if name == "" {
			panic("expected a name")
		}

		// TODO: Validate the name here.

		fmt.Printf("name: %s\n", name)
		chiCtx := r.Context().Value(chi.RouteCtxKey).(*chi.Context)

		// Assigning to RoutePath is what overrides the routing.
		chiCtx.RoutePath = strings.Replace(r.URL.Path, name, "_name", 1)
		fmt.Printf("route path: %s\n", chiCtx.RoutePath)

		ctx := context.WithValue(r.Context(), nameKey, name)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Returns "" when no name is found.
func extractNameFromV2URL(path string) string {
	matches := v2NamePat.FindStringSubmatch(path)
	// This method can return nil, isn't that nice?
	if matches == nil || len(matches) < 2 {
		return ""
	}

	return matches[1]
}
